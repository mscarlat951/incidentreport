package ro.bigdata.project.incidentreport.util;


public class Constants {

    // Web address
    public static final String INCIDENT_REPORT_WEB_ADDRESS = "https://www.google.ro/";

    // TODO: Modify Server addresses
    public static final String URL_POST_REQ = "http://10.42.0.1:5000/api/v1/incidents";

    // Request permissions
    public static final int PERMISSIONS_REQUEST_CODE = 8;

    // Request codes
    public static final int CAMERA_REQUEST_CODE = 9;

    // Location Service
    public static final long LOCATION_UPDATE_INTERVAL_MS = 1;
    public static final float LOCATION_UPDATE_DISTANCE_METERS = 0.5f;
    public static final String LOCATION_UPDATE_ACTION = "location.update.action";
    public static final String LOCATION_UPDATE_LATITUDE = "location.update.latitude";
    public static final String LOCATION_UPDATE_LONGITUDE = "location.update.longitude";
}
