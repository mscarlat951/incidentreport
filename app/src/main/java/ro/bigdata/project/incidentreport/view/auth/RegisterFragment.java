package ro.bigdata.project.incidentreport.view.auth;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.app.UserAuth;
import ro.bigdata.project.incidentreport.model.User;
import ro.bigdata.project.incidentreport.view.auth.LoginActivity;

public class RegisterFragment extends android.support.v4.app.Fragment {

    private LoginActivity activity;
    private View rootView;
    private EditText emailEditText, passwordEditText, nameEditText;
    private Button submitButton;

    public RegisterFragment(LoginActivity activity) {
        this.activity = activity;
    }

    private View.OnClickListener submitBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String email = emailEditText.getText().toString();
            final String password = passwordEditText.getText().toString();
            final String name = nameEditText.getText().toString();

            if (name.trim().length() == 0 || email.trim().length() == 0 || password.trim().length() == 0) {
                Snackbar.make(submitButton, "You cannot leave empty fields!", Snackbar.LENGTH_SHORT).show();
                return;
            }

            register(name, email, password);
        }
    };

    private void register(String name, String email, String password) {
        // TODO: save credentials
        UserAuth.setUser(new User(
                name,
                email,
                password
        ));

        activity.startMainActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register, container, false);

        nameEditText = rootView.findViewById(R.id.name_edit_text);
        emailEditText = rootView.findViewById(R.id.email_edit_text);
        passwordEditText = rootView.findViewById(R.id.password_edit_text);
        submitButton = rootView.findViewById(R.id.submit_button);

        submitButton.setOnClickListener(submitBtnClickListener);

        return rootView;
    }
}
