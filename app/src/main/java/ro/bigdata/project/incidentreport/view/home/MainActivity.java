package ro.bigdata.project.incidentreport.view.home;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.app.UserAuth;
import ro.bigdata.project.incidentreport.service.location.LocationService;
import ro.bigdata.project.incidentreport.util.Constants;
import ro.bigdata.project.incidentreport.view.incident_report.ReportIncidentFragment;
import ro.bigdata.project.incidentreport.view.timeline.TimelineFragment;
import ro.bigdata.project.incidentreport.view.profile_settings.UserProfileFragment;
import ro.bigdata.project.incidentreport.view.about.AboutUsFragment;
import ro.bigdata.project.incidentreport.view.auth.LoginActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    /* App Toolbar */
    private Toolbar toolbar;

    /* Navigation Menu */
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle drawerToggle;

    /* Current fragment info */
    private boolean navigationItemSelected;
    private Fragment selectedFragment;
    private String fragmentTag;

    /* Navigation View Listener */
    private NavigationView.OnNavigationItemSelectedListener navigationViewListener =
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    final int id = item.getItemId();
                    navigationItemSelected = true;

                    /* Obtain the selected fragment */
                    fragmentTag = MainFragmentsFactory.getFragmentTag(getApplicationContext(), id);
                    selectedFragment = MainFragmentsFactory.getCurrentFragment(id);

                    /* Alert user if he wants to logout */
                    if (selectedFragment == null && fragmentTag == null && id == R.id.logout_item_menu) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Are you sure you want to logout?")
                                .setCancelable(true)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        logout();
                                    }
                                })
                                .setNegativeButton("No", null)
                                .create()
                                .show();
                    }

                    /* Hide navigation menu */
                    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        drawerLayout.closeDrawer(GravityCompat.START);
                    }

                    return false;
                }
            };

    /* Drawer Layout Listener */
    private DrawerLayout.DrawerListener drawerLayoutListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        }

        @Override
        public void onDrawerOpened(@NonNull View drawerView) {
        }

        @Override
        public void onDrawerClosed(@NonNull View drawerView) {
            if (navigationItemSelected) {
                navigationItemSelected = false;

                if (selectedFragment != null && fragmentTag != null) {
                    replaceCurrentFragment(selectedFragment, fragmentTag);
                }
            }
        }

        @Override
        public void onDrawerStateChanged(int newState) {
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    public void setToolbarTitle(final String title) {
        if (toolbar == null)
            return;

        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerToggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Toolbar */
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        /* Container View */
        drawerLayout = (DrawerLayout) findViewById(R.id.root_drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);

        /* Add fragments */
        selectedFragment = new ReportIncidentFragment();
        fragmentTag = getResources().getString(R.string.report_incident);
        addFragment(selectedFragment, fragmentTag);

        /* Navigation menu */
        navigationView = (NavigationView) findViewById(R.id.menu_navigation_view);
        navigationView.setNavigationItemSelectedListener(navigationViewListener);
        drawerLayout.addDrawerListener(drawerLayoutListener);

        /* Start location service */
        if (checkPermissions() && !LocationService.getStatus())
            startService(new Intent(MainActivity.this, LocationService.class));
    }

    private boolean checkPermissions() {
        /* Check build version */
        final int buildVersion = Build.VERSION.SDK_INT;
        Log.e(TAG, "checkPermissions: BuildVersion is " + buildVersion);

        if (buildVersion <= Build.VERSION_CODES.M) {
            Log.e(TAG, "checkPermissions: Not required");
            return true;
        }

        /* Check if permissions are already granted */
        final String[] requiredPermissions = {
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };

        final ArrayList<String> requestPermissions = new ArrayList<>();

        for (String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions.add(permission);
            }
        }

        /* Ask for permissions if there are no granted yet */
        if (requestPermissions.size() > 0) {
            Log.e(TAG, "checkPermissions: Asking for " + requestPermissions);
            ActivityCompat.requestPermissions(
                    this,
                    requestPermissions.toArray(new String[requestPermissions.size()]),
                    Constants.PERMISSIONS_REQUEST_CODE);
            return false;
        }

        return true;
    }

    private void addFragment(Fragment fragment, String TAG) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container_frame_layout, fragment, TAG)
                .commit();

        setToolbarTitle(TAG);
    }

    private void replaceCurrentFragment(Fragment fragment, String TAG) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.container_frame_layout, fragment, TAG)
                .addToBackStack(null)
                .commit();

        setToolbarTitle(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();

        /* If user is not logged in, then proceed to the login page */
        if (!UserAuth.userIsLogged()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
         /* Stop location service */
        if (LocationService.getStatus())
            stopService(new Intent(MainActivity.this, LocationService.class));

        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(getClass().getName(), "Permissions Granted");

                    if (!LocationService.getStatus())
                        startService(new Intent(MainActivity.this, LocationService.class));
                }
                break;

            default:
                break;
        }
    }

    private void logout() {
        UserAuth.setUser(null);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}