package ro.bigdata.project.incidentreport.service.location;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;


class LocationUpdatesListener implements LocationListener {

    private LocationUpdatesManager manager = null;

    LocationUpdatesListener(LocationUpdatesManager manager) {
        this.manager = manager;
    }

    @Override
    public void onLocationChanged(Location location) {
        manager.notifyLocationChanged(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}


    @Override
    public void onProviderDisabled(String provider) {
        switch (provider) {
            case LocationManager.GPS_PROVIDER:
                break;
            case LocationManager.NETWORK_PROVIDER:
                break;
        }
    }
}
