package ro.bigdata.project.incidentreport.view.home;

import android.content.Context;
import android.support.v4.app.Fragment;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.view.about.AboutUsFragment;
import ro.bigdata.project.incidentreport.view.incident_report.ReportIncidentFragment;
import ro.bigdata.project.incidentreport.view.profile_settings.UserProfileFragment;


public class MainFragmentsFactory {
    private final static Fragment userProfileFragment = new UserProfileFragment();
    private final static Fragment reportIncidentFragment = new ReportIncidentFragment();
    private final static Fragment aboutUsFragment = new AboutUsFragment();
//    private final static Fragment timelineFragment = new TimelineFragment();

    public static Fragment getCurrentFragment(int id) {
        switch (id) {
            case R.id.profile_item_menu:
                return userProfileFragment;
            case R.id.report_incident_item_menu:
                return  reportIncidentFragment;
            case R.id.about_item_menu:
                return aboutUsFragment;
//            case R.id.timeline_item_menu:
//                return timelineFragment;
        }

        return null;
    }

    public static String getFragmentTag(Context context, int id) {

        switch (id) {
            case R.id.profile_item_menu:
                return context.getResources().getString(R.string.profile_settings);
            case R.id.report_incident_item_menu:
                return context.getResources().getString(R.string.report_incident);
            case R.id.about_item_menu:
                return context.getResources().getString(R.string.about_us);
//            case R.id.timeline_item_menu:
//                return context.getResources().getString(R.string.timeline);
        }

        return null;
    }
}
