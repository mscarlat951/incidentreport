package ro.bigdata.project.incidentreport.view.auth;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.app.UserAuth;
import ro.bigdata.project.incidentreport.model.User;
import ro.bigdata.project.incidentreport.view.auth.LoginActivity;

public class LoginFragment extends android.support.v4.app.Fragment {

    private LoginActivity activity;
    private View rootView;
    private EditText emailEditText, passwordEditText;
    private Button submitButton;
    private TextView guestTextView;

    public LoginFragment(LoginActivity activity) {
        this.activity = activity;
    }

    private View.OnClickListener submitBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String email = emailEditText.getText().toString();
            final String password = passwordEditText.getText().toString();

            if (email.trim().length() == 0 || password.trim().length() == 0) {
                Snackbar.make(submitButton, "You cannot leave empty fields!", Snackbar.LENGTH_SHORT).show();
                return;
            }

            login(email, password);
        }
    };

    private View.OnClickListener guestBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            login(null, null);
        }
    };


    private void login(String email, String password) {
        // TODO: save credentials; Add logic here
        UserAuth.setUser(new User(
                null,
                email,
                password
        ));

        activity.startMainActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);

        emailEditText = rootView.findViewById(R.id.email_edit_text);
        passwordEditText = rootView.findViewById(R.id.password_edit_text);
        submitButton = rootView.findViewById(R.id.submit_button);
        guestTextView = rootView.findViewById(R.id.guest_text_view);


        submitButton.setOnClickListener(submitBtnClickListener);
        guestTextView.setOnClickListener(guestBtnClickListener);

        return rootView;
    }
}
