package ro.bigdata.project.incidentreport.view.incident_report;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.api.http_req.PostRequestTask;
import ro.bigdata.project.incidentreport.util.Constants;
import ro.bigdata.project.incidentreport.util.FileUtil;
import ro.bigdata.project.incidentreport.util.LocationUtil;

public class ReportIncidentFragment  extends android.support.v4.app.Fragment {
    private static final String TAG = "ReportIncidentFragment";

    private View rootView;
    private FrameLayout photoLayout;
    private ImageView photoImageView;
    private ImageButton takePhotoButton;
    private EditText dateEditText, locationEditText, descriptionEditText, typeEditText;
    private Spinner codeAlertSpinner;
    private AutoCompleteTextView alertCategoryTextView;
    private Button submitButton;
    private Uri photoUri;
    private Location userLocation = new Location("gps"); // provider doesn't matter here

    private class PhotoClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult( Intent.createChooser(intent, "Select Picture"),
                                    Constants.CAMERA_REQUEST_CODE);
        }
    }

    private class SubmitReportListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String date = dateEditText.getText().toString();
            final String location = locationEditText.getText().toString();
            final String type = typeEditText.getText().toString();
            final String codeAlert = codeAlertSpinner.getSelectedItem().toString();
            final String description = descriptionEditText.getText().toString();
            final String[] tags = alertCategoryTextView.getText().toString().split(", ");

            /* Check inputs */
            if (date.equalsIgnoreCase("") || location.equalsIgnoreCase("") ||
                    type.equalsIgnoreCase("") ||  description.equalsIgnoreCase("") ||
                    tags.length == 0) {

                Snackbar.make(v, "You cannot leave empty fields!", Snackbar.LENGTH_SHORT).show();
                return;
            }

            sendReport(
                    date,
                    userLocation,
                    type,
                    codeAlert,
                    description,
                    FileUtil.getByteArrFromURI(getContext(), photoUri),
                    tags
            );
        }
    }

    // Location Updates receiver
    private IntentFilter locationIntentFilter = new IntentFilter(Constants.LOCATION_UPDATE_ACTION);
    private BroadcastReceiver locationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final double latitude = intent.getDoubleExtra(Constants.LOCATION_UPDATE_LATITUDE, -1);
            final double longitude = intent.getDoubleExtra(Constants.LOCATION_UPDATE_LONGITUDE, -1);

            /* Save anyway location info */
            userLocation.setLongitude(longitude);
            userLocation.setLatitude(latitude);

            if (latitude == -1 || longitude == -1)
                return;

            final String decodedAddress = LocationUtil.decodeAddress(getActivity(), latitude, longitude);

            boolean isLocationNotificationEnabled = locationEditText.getText().toString().isEmpty() &&
                    decodedAddress != null && !decodedAddress.equalsIgnoreCase("");

            if (isLocationNotificationEnabled) {
                locationEditText.setText(decodedAddress);
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        rootView = inflater.inflate(R.layout.fragment_report_incident, container, false);
        dateEditText = rootView.findViewById(R.id.date_edit_text);
        typeEditText = rootView.findViewById(R.id.type_edit_text);
        locationEditText = rootView.findViewById(R.id.location_edit_text);
        photoLayout = rootView.findViewById(R.id.photo_layout);
        takePhotoButton = rootView.findViewById(R.id.take_photo_button);
        photoImageView = rootView.findViewById(R.id.photo_image_view);
        descriptionEditText = rootView.findViewById(R.id.description_edit_text);
        codeAlertSpinner = rootView.findViewById(R.id.code_alert_spinner);
        alertCategoryTextView = rootView.findViewById(R.id.alert_category_text_view);
        submitButton = rootView.findViewById(R.id.submit_button);

        // Setup listeners
        photoLayout.setOnClickListener(new PhotoClickListener());
        submitButton.setOnClickListener(new SubmitReportListener());
        setAutoCompleteAlertTypes();

        return rootView;
    }

    private void sendReport(String date, Location location, String type, String codeAlert,
                            String description, byte[] photoByteAr, String[] tags) {
        try {

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("type", type);
            jsonObject.put("description", description);
            jsonObject.put("latitude", location.getLatitude());
            jsonObject.put("longitude", location.getLongitude());

            JSONArray jsonTags = new JSONArray();
            for (String tag : tags) {
                jsonTags.put(tag);
            }

            jsonObject.put("tags", jsonTags);

            new PostRequestTask().execute(
                    jsonObject.toString(),
                    new String(photoByteAr)
            );

            Toast.makeText(getActivity(), "Your report has been successfully sent to us!",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Encountered Exception in sending report: " + e.getMessage());
            Toast.makeText(getActivity(), "We have encountered some problems. Please, try again later.",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void autoFillDateTime() {
        Date currentTime = Calendar.getInstance().getTime();

        dateEditText.setText(android.text.format.DateFormat.format(
                "yyyy-MM-dd HH:mm",
                currentTime
        ));
    }

    private void setAutoCompleteAlertTypes() {
        String[] alertTypes = getResources().getStringArray(R.array.alert_type);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                alertTypes
        );

        alertCategoryTextView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(locationBroadcastReceiver, locationIntentFilter);
        autoFillDateTime();
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(locationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.CAMERA_REQUEST_CODE) {
                photoUri = data.getData();
                Bitmap bitmap = null;

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(
                            getActivity().getContentResolver(),
                            photoUri);

                } catch (IOException e) {
                    Log.e(TAG, "Extract image from uri Exception: " + e.getMessage());
                }

                if (bitmap != null) {
                    takePhotoButton.setVisibility(View.GONE);
                    photoImageView.setVisibility(View.VISIBLE);
                    photoImageView.setImageBitmap(bitmap);
                }
            }
        }
    }
}
