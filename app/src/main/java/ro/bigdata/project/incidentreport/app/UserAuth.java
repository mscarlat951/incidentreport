package ro.bigdata.project.incidentreport.app;


import ro.bigdata.project.incidentreport.model.User;

public class UserAuth {

    /* Singleton Instance */
    private static UserAuth instance = new UserAuth();

    private static User user;

    private UserAuth() {}

    public static boolean userIsLogged() {
        return user != null;
    }

    public static void setUser(User user) {
        UserAuth.user = user;
    }

    public static User getUser() {
        return UserAuth.user;
    }
}