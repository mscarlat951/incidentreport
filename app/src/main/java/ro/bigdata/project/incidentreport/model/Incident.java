package ro.bigdata.project.incidentreport.model;

public class Incident {

    private String id;

    private User publisher;
    private String date;
    private String location;
    private String codeAlert;
    private String description;
    private byte[] photo;
    private String category;

    public Incident(User publisher, String date, String location, String codeAlert,
                    String description, byte[] photo, String category) {
        this.publisher = publisher;
        this.date = date;
        this.location = location;
        this.codeAlert = codeAlert;
        this.description = description;
        this.photo = photo;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public User getPublisher() {
        return publisher;
    }

    public String getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public String getCodeAlert() {
        return codeAlert;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getCategory() {
        return category;
    }
}
