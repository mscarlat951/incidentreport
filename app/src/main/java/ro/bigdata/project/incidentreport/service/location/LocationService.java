package ro.bigdata.project.incidentreport.service.location;

import android.app.Service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


public class LocationService extends Service {
    private static final String TAG = "LocationService";

    private LocationUpdatesManager locationUpdatesManager;
    private static boolean status = false;

    public static boolean getStatus() {
        return LocationService.status;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service started.");

        setup();
        status = true;

        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service stopped");

        locationUpdatesManager.stopLocationUpdates();
        status = false;

        super.onDestroy();
    }

    private void setup() {
        locationUpdatesManager = new LocationUpdatesManager(this);

        locationUpdatesManager.setup();
        locationUpdatesManager.getLastLocation();
    }
}