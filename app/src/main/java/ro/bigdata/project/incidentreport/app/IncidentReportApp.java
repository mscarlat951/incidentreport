package ro.bigdata.project.incidentreport.app;

import android.app.Application;
import android.util.Log;

public class IncidentReportApp extends Application {
    private static final String TAG = "IncidentReportApp";

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: Method was invoked");
        super.onCreate();
    }

    @Override
    public void onLowMemory() {
        Log.d(TAG, "onLowMemory: Method was invoked!");
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        Log.d(TAG, "onTerminate: Method was invoked!");
        super.onTerminate();
    }
}
