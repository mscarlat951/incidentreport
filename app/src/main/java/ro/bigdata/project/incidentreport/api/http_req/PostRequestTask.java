package ro.bigdata.project.incidentreport.api.http_req;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import ro.bigdata.project.incidentreport.util.Constants;

/**
 * Created by marius on 1/11/19.
 */

public class PostRequestTask extends AsyncTask<
        String, /* Input params */
        Void, /* Progress */
        Void /* Result */ > {

    private static final String TAG = "PostRequestTask";

    @Override
    protected Void doInBackground(String... params) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.URL_POST_REQ);

        try {

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            entity.addPart("data", new StringBody(params[0]));
            entity.addPart("image", new ByteArrayBody(params[1].getBytes(), "image.jpg"));

            httppost.setEntity(entity);

            httpclient.execute(httppost);

        } catch (Exception e) {
            Log.e(TAG, "Encountered Exception: " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
