package ro.bigdata.project.incidentreport.view.profile_settings;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import de.hdodenhof.circleimageview.CircleImageView;
import ro.bigdata.project.incidentreport.R;

public class UserProfileFragment  extends android.support.v4.app.Fragment {

    private View rootView;
    private CircleImageView profileImageView;
    private EditText nameEditText, emailEditText, phoneNumberEditText, locationEditText;
    private ImageButton editInformationButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_user_profile, container, false);

        /* Setup Views */
        profileImageView = rootView.findViewById(R.id.profile_image);
        nameEditText = rootView.findViewById(R.id.name_edit_text);
        emailEditText = rootView.findViewById(R.id.email_edit_text);
        phoneNumberEditText = rootView.findViewById(R.id.phone_number_edit_text);
        locationEditText = rootView.findViewById(R.id.location_edit_text);
        editInformationButton = rootView.findViewById(R.id.edit_information_button);

        /* Add Listeners */
        editInformationButton.setOnClickListener(editUserInfoClickListener);
        profileImageView.setOnClickListener(profileImageListener);

        /* Disable editing for user information */
        setUserInfoEditable(false);

        return rootView;
    }

    private void setUserInfoEditable(boolean active) {
        nameEditText.setEnabled(active);
        emailEditText.setEnabled(active);
        locationEditText.setEnabled(active);
        phoneNumberEditText.setEnabled(active);
    }

    private View.OnClickListener editUserInfoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean informationEditable =
                    nameEditText.isEnabled() && emailEditText.isEnabled() &&
                    locationEditText.isEnabled() && phoneNumberEditText.isEnabled();

            if (informationEditable) {
                setUserInfoEditable(false);
                editInformationButton.setImageResource(R.drawable.editbtn);
            } else {
                setUserInfoEditable(true);
                editInformationButton.setImageResource(R.drawable.savebtn);
                nameEditText.requestFocus();
            }
        }
    };

    private View.OnClickListener profileImageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String[] imageProviders = {
                    "Take a photo using camera",
                    "Import from gallery"
            };

            new AlertDialog.Builder(getActivity())
                    .setTitle("Replace Your Photo")
                    .setItems(imageProviders, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0: /* Camera */
                                    break;

                                case 1: /* Gallery */
                                    break;
                            }
                        }
                    })
                    .create()
                    .show();
        }
    };

}
