package ro.bigdata.project.incidentreport.service.location;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

import java.util.List;

import ro.bigdata.project.incidentreport.util.Constants;


public class LocationUpdatesManager {

    private static final String TAG = "LocationUpdatesManager";

    // Location Util
    private LocationManager mLocationManager;
    private LocationUpdatesListener networkLocationListener;
    private LocationUpdatesListener gpsLocationListener;

    // Caller Context
    private Context context;

    Context getContext() {
        return context;
    }

    LocationUpdatesManager(Context context) {
        this.context = context;
    }

    void setup() {
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        networkLocationListener = new LocationUpdatesListener(this);
        gpsLocationListener = new LocationUpdatesListener(this);

        requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                gpsLocationListener
        );

        requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                networkLocationListener
        );
    }

    @SuppressWarnings({"MissingPermission"})
    private void requestLocationUpdates(String locationProvider, LocationListener locationListener) {
        mLocationManager.requestLocationUpdates(
                locationProvider,
                Constants.LOCATION_UPDATE_INTERVAL_MS,
                Constants.LOCATION_UPDATE_DISTANCE_METERS,
                locationListener
        );
    }

    void notifyLocationChanged(Location location) {
        Log.d(TAG, "notifyLocationChanged: " + location.getLatitude() + " ; " + location.getLongitude());
        final Intent intent = new Intent();

        intent.setAction(Constants.LOCATION_UPDATE_ACTION);
        intent.putExtra(Constants.LOCATION_UPDATE_LATITUDE, location.getLatitude());
        intent.putExtra(Constants.LOCATION_UPDATE_LONGITUDE, location.getLongitude());

        context.sendBroadcast(intent);
    }

    @SuppressWarnings({"MissingPermission"})
    void getLastLocation() {

        try {

            mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);
            Log.e(TAG, "getLastLocation Providers: " + providers);

            Location bestLocation = null;

            for (String provider : providers) {

                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null)
                    continue;

                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy())
                    bestLocation = l;
            }

            Log.e(TAG, "getLastLocation: Finished with " + bestLocation);

            if (bestLocation != null)
                notifyLocationChanged(bestLocation);

        } catch (Exception e) {
            Log.e(TAG, "getLastLocation Exception: " + e.getMessage());
        }
    }

    void stopLocationUpdates() {
        mLocationManager.removeUpdates(gpsLocationListener);
        mLocationManager.removeUpdates(networkLocationListener);
        mLocationManager = null;
    }

}
