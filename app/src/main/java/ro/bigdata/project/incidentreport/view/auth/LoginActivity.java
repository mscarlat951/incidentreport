package ro.bigdata.project.incidentreport.view.auth;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.util.Constants;
import ro.bigdata.project.incidentreport.view.home.MainActivity;

public class LoginActivity extends AppCompatActivity {

    private TabLayout authTabLayout;
    private AppBarLayout authAppBar;
    private ViewPager authViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authTabLayout = findViewById(R.id.auth_tab_layout);
        authAppBar = findViewById(R.id.auth_app_bar);
        authViewPager = findViewById(R.id.auth_view_pager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new LoginFragment(this), getResources().getString(R.string.login));
        adapter.addFragment(new RegisterFragment(this), getResources().getString(R.string.register));

        authViewPager.setAdapter(adapter);
        authTabLayout.setupWithViewPager(authViewPager);
    }

    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
