package ro.bigdata.project.incidentreport.view.about;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import ro.bigdata.project.incidentreport.R;
import ro.bigdata.project.incidentreport.util.Constants;


public class AboutUsFragment extends android.support.v4.app.Fragment {

    private View rootView;
    private WebView aboutUsWebView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about_us, container, false);


        /* Setup link to the reference page */
        aboutUsWebView = rootView.findViewById(R.id.about_us_webview);

        /* Apply Web Settings */
        WebSettings webSettings = aboutUsWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        aboutUsWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        aboutUsWebView.getSettings().setBuiltInZoomControls(true);
        aboutUsWebView.getSettings().setUseWideViewPort(true);
        aboutUsWebView.getSettings().setLoadWithOverviewMode(true);

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        progressDialog.setMessage("Loading content ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        aboutUsWebView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), "Error: " + description, Toast.LENGTH_SHORT).show();
            }
        });

        aboutUsWebView.loadUrl(Constants.INCIDENT_REPORT_WEB_ADDRESS);


        return rootView;
    }
}
