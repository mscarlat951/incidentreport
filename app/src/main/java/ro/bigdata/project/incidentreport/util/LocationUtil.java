package ro.bigdata.project.incidentreport.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;

/**
 * Created by marius on 11/15/18.
 */

public class LocationUtil {

    public static String decodeAddress(Context context, double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(context);
        String address;

        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);

            address =   addressList.get(0).getLocality() + ", " +
                        addressList.get(0).getCountryName();
        } catch (IOException e) {
            address = "";
        }

        return address;
    }


}
