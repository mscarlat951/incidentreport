package ro.bigdata.project.incidentreport.util;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class FileUtil {

    public static String getPathFromUri(Uri uri) {
        return null;
    }

    private static final String TAG = "FileUtil";

    public static byte[] getByteArrFromURI(Context context, Uri photoUri) {
        byte[] photoByteArr = null;

        try {
            InputStream inputStream = context.getContentResolver().openInputStream(photoUri);
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            int len = 0;
            while ((len = inputStream.read(buffer)) != -1)
                byteBuffer.write(buffer, 0, len);

            photoByteArr = byteBuffer.toByteArray();

        } catch (Exception e) {
            Log.d(TAG, "Encountered Exception: " + e.getMessage());
        } finally {
            Log.d(TAG, "Obtain photo byte arr value: Diff by null ==> " + (photoByteArr != null));
        }


        return photoByteArr;
    }
}
